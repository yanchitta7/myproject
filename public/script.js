let props;
let metaData;

// Fetch JSON data
fetch('../data/example.json')
    .then(response => response.json())
    .then(data => {
        metaData = data.meta;
        props = data.props;

        //populate UI elements
        setupDropdown(metaData.styling);
        setupCheckbox(metaData.openTermsInCustomLightbox);
        setupRadioButton(metaData.promoCodeDisplay);
        setupInput(metaData.promoCodeSize);
    })
    .catch(error => {
        console.error('Error fetching data. ' + error);
    });

/**
 * Populates dropdown UI element
 * @param {Object} stylingOptions 
 */
function setupDropdown(stylingOptions) {
    const dropdown = document.getElementById("styling");
    const { values, defaultValue, description, label } = stylingOptions;

    values.forEach(option => {
        const key = Object.keys(option)[0];
        const optionValue = option[key];

        const optionElement = document.createElement("option");
        optionElement.setAttribute("class", "select-items");
        optionElement.value = optionValue;
        optionElement.textContent = optionValue;

        if (optionValue === defaultValue) {
            optionElement.selected = true;
        }

        dropdown.appendChild(optionElement);
    });

    const descriptionText = document.getElementById("stylingDescription");
    const labelElement = document.getElementById("stylingLabel");

    //Set styling description and label
    descriptionText.textContent = description;
    labelElement.textContent = label;
}
/**
 * Sets up the checkbox UI element
 * @param {Object} checkboxData 
 */
function setupCheckbox(checkboxData) {
    const { label, defaultValue, description } = checkboxData;

    const checkbox = document.getElementById("openTerms");
    const labelElement = document.getElementById("openTermsLabel");
    const descriptionText = document.getElementById("openTermsDescription");

    checkbox.checked = JSON.parse(defaultValue);
    labelElement.textContent = label;
    descriptionText.textContent = description;
}
/**
 * Sets up radio buttons UI elements
 * 
 * @param {Object} promoCodeDisplayData 
 */
function setupRadioButton(promoCodeDisplayData) {
    const { description, defaultValue, values, label } = promoCodeDisplayData;

    const radioContainer = document.getElementById("promoCodeDisplay");
    const labelElement = document.getElementById("labelRadio");
    const descriptionText = document.getElementById("radioDescription");

    labelElement.textContent = label;
    descriptionText.textContent = description;

    values.forEach(option => {
        const key = Object.keys(option)[0];
        const optionValue = option[key];

        const radioLabel = document.createElement("label");
        radioLabel.textContent = optionValue;

        const radioButton = document.createElement("input");
        radioButton.type = "radio";
        radioButton.name = "radio";
        radioButton.value = optionValue;

        if (optionValue === defaultValue) {
            radioButton.checked = true;
        }

        radioLabel.appendChild(radioButton);
        radioContainer.appendChild(radioLabel);
    });
}

/**
 * Set up input UI element
 * 
 * @param {Object} promoCodeSizeData 
 */
function setupInput(promoCodeSizeData) {
    const { description, defaultValue, label } = promoCodeSizeData

    const inputElement = document.getElementById("promoCodeSize");
    const labelElement = document.getElementById("promoCodeSizeLabel");
    const descriptionText = document.getElementById("promoCodeSizeDescription");

    inputElement.textContent = defaultValue;
    labelElement.textContent = label;
    descriptionText.textContent = description;
}

document.addEventListener("DOMContentLoaded", function () {
    const saveButton = document.getElementById("save");

    // Saves user preferences
    function handleSave(event) {
        event.preventDefault();
        dropdownPreferences();
        checkboxPreferences();
        radioButtonsPreferences();
        inputPreferences();
        outputProps();
    }
    saveButton.addEventListener("click", handleSave);

    const resetButton = document.getElementById("reset");
    // restores user preferences to saved state
    function handleReset(event) {
        event.preventDefault();
        setSelected(props.styling);
        setOpenTerms(props.openTermsInCustomLightbox);
        setRadioButton(props.promoCodeDisplay);
        setPromoCodeSize(props.promoCodeSize);
    }
    resetButton.addEventListener("click", handleReset);

    const setDefaultsButton = document.getElementById("defaults");
    // sets user preferences to default state
    function setDefaults(event) {
        event.preventDefault();
        setSelected(metaData.styling.defaultValue);
        setOpenTerms(metaData.openTermsInCustomLightbox.defaultValue);
        setRadioButton(metaData.promoCodeDisplay.defaultValue);
        setPromoCodeSize(metaData.promoCodeSize.defaultValue)
    }
    setDefaultsButton.addEventListener("click", setDefaults);

});

// Updates the props for styling based on the selected option in the dropdown 
function dropdownPreferences() {
    // Get the selected value from "styling" dropdown
    const selectedValue = document.getElementById("styling").value;
    // Find the corresponding option key in the metadata
    metaData.styling.values.forEach(option => {
        const key = Object.keys(option)[0];
        if (option[key] === selectedValue) {
            // Update the props with the selected styling option key
            props.styling = key;
        }
    });
}

// Updates the props for openTermsInCustomLightbox based on the checkbox state
function checkboxPreferences() {
    const openTerms = document.getElementById("openTerms");
    props.openTermsInCustomLightbox = openTerms.checked;
}

// Updates the props for promoCodeDisplay based on selected radio button
function radioButtonsPreferences() {
    // Get all radio buttons
    const radioElements = document.querySelectorAll('input[name="radio"]');
    // Iterate over radio buttons and update props if checked
    for (const radioButton of radioElements) {
        if (radioButton.checked) {
            metaData.promoCodeDisplay.values.forEach(option => {
                const key = Object.keys(option)[0];
                if (option[key] === radioButton.value) {
                    props.promoCodeDisplay = key;
                }
            })
        }
    }
}

// Updates the props for promoCodeSize based on the input field data
function inputPreferences() {
    const promoCodeSize = document.getElementById("promoCodeSize");
    props.promoCodeSize = promoCodeSize.value;
}

//Displays the props
function outputProps () {
    const outputTextarea = document.getElementById("output");
    const outputContainer = document.querySelector(".container.output");
    outputTextarea.style.height = "fit-content";
    outputTextarea.style.maxWidth = "260px";
    outputTextarea.style.wordBreak = "break-word"
    outputContainer.style.display = "flex";
    outputTextarea.textContent = "styling: " + props.styling + "\nopenTermsInCustomLightbox: " + props.openTermsInCustomLightbox +
    "\npromoCodeDisplay: " + props.promoCodeDisplay + "\npromoCodeSize: " + props.promoCodeSize;
}

/**
 * Sets the selected option in the styling dropdown based on the given value
 * 
 * @param {string} newValue 
 */
function setSelected(newValue) {
    //Get the dropdown element
    const styling = document.getElementById("styling");
    //Iterate over metadata to find the matching option
    metaData.styling.values.forEach(style => {
        const key = Object.keys(style)[0];
        if (key === newValue) {
            // Iterate through dropdown options to set the selected option
            for (const option of styling.options) {
                if (option.value === style[key]) {
                    option.selected = true;
                }
            }
        }
    })
}

/**
 * Set the selected radio button based on the given value for promo code display
 * @param {string} newValue 
 */
function setRadioButton(newValue) {
    const radioElements = document.querySelectorAll('input[name="radio"]');
    metaData.promoCodeDisplay.values.forEach(code => {
        const key = Object.keys(code)[0];
        if (key === newValue) {
            for (const radio of radioElements) {
                if (radio.value === code[key]) {
                    radio.checked = true;
                }
                else {
                    radio.checked = false;
                }
            }
        }
    });
}

/**
 * Set the state of the "openTerms" checkbox based on the provided value
 * 
 * @param {string} newValue 
 */
function setOpenTerms(newValue) {
    const openTerms = document.getElementById("openTerms");
    openTerms.checked = JSON.parse(newValue);
}

/**
 * Set the value of the promo code size input based on the provided value
 * 
 * @param {string} newValue 
 */
function setPromoCodeSize(newValue) {
    const promoCodeSize = document.getElementById("promoCodeSize");
    promoCodeSize.value = newValue;
}
